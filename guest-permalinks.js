// college-specific variables
var college = document.getElementById('collegeID').innerHTML;
var custid;
if (college)
{
    switch (college)
    {
    case 'arc':
        custid = 'amerriv';
        break;
    case 'crc':
        custid = 'cosum';
        break;
    case 'flc':
        custid = 'ns015092';
        break;
    case 'scc':
        custid = 'sacram';
    }

    (function ()
    { // provide guest permalinks in EDS. needs to be rewritten because I'm repeating a few things
        var proxyString = '0-search.ebscohost.com.lasiii.losrios.edu';
        var deProxyString = 'search.ebscohost.com';
        var guestParams = 'authtype=ip,guest&custid=' + custid + '&groupid=main&profile=eds&direct=true';
        // Detailed Records
        if (ep.clientData.plink)
        {
            ep.clientData.plink = ep.clientData.plink.replace(proxyString, deProxyString);
            ep.clientData.plink = ep.clientData.plink.replace('direct=true', guestParams);
        }
        // Results screens
        var proxypermalink = document.getElementById('pLink');
        if (proxypermalink)
        {
            var plinkvalue = proxypermalink.value;
            plinkvalue = plinkvalue.replace(proxyString, deProxyString);
            plinkvalue = plinkvalue.replace('direct=true', guestParams);
            proxypermalink.value = plinkvalue;
        }
    })();
}