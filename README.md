## NO LONGER UPDATED ##
This has been integrated into the EDS-Complete repository. Any further changes will be made there.

* * *


EDS Guest Permalinks
===
EBSCO does not currently allow us to use guest permalinks in detailed records and results pages. This script, inserted into bottom branding, simply changes the permalinks to allow guest authentication. This is particularly useful for catalog items, which should not require login.

If the record requires authentication, it will send the user to the proxy server.

Guest parameter
---
This is to be used in a 4-college setting, where each college has its own profile. Each bottom branding has a div indicating which college it is, like so:
````
<div id="collegeID" style="display:none;">scc</div>
````
This way the script can be hosted externally and when it needs updating only needs to be updated once.

Permalink String on Detailed Records Page
---
EBSCO provides a kind of token-based access to certain strings. `Ep.ClientData.plink` is the permalink string on the Detailed Records page.